var gulp 			          = require('gulp');
var connect 	          = require('gulp-connect');
var historyApiFallback  = require('connect-history-api-fallback');
var inject              = require('gulp-inject');
var jshint              = require('gulp-jshint');
var debug               = require('gulp-debug');
var wiredep             = require('wiredep').stream;

var prefixer 						= require('gulp-autoprefixer');
var sass 								= require('gulp-ruby-sass');
var sourcemaps 					= require('gulp-sourcemaps');

gulp.task('server', function() {
   connect.server({
     root: './client',
     hostname: '0.0.0.0',
     port: 3000,
     livereload: true
  });
});

gulp.task('html', function() {
  gulp.src('./client/pages/**/*.html')
    .pipe(connect.reload());
});

gulp.task('styles', function() {
  return gulp.src('./client/scss/*.scss')
  .pipe(sass({
    noCache		: true,
    style		: "compact" // compact, nested
  }))
  .pipe(gulp.dest('./client/css'))
  .pipe(connect.reload())
  .pipe(prefixer(['last 2 versions'], {cascade: true }));
});

gulp.task('inject', function() {
  return gulp.src('./client/pages/**/*.html')
    .pipe(inject(gulp.src('./client/css/*.css', {read: false}), {relative: true}))
    .pipe(inject(gulp.src(['./client/js/jquery.min.js', './client/js/highcharts.js'], {read: false}), {name: 'priority', relative: true}))

    .pipe(inject(gulp.src(['./client/js/*.js', '!./client/js/jquery.min.js', '!./client/js/highcharts.js'], {read: false}), {relative: true}))
    .pipe(gulp.dest('./client/pages'));
});

gulp.task('wiredep', function () {
 gulp.src('./client/pages/**/*.html')
   .pipe(wiredep())
   .pipe(gulp.dest('./client/pages'));
});

gulp.task('watch', function() {
  gulp.watch(["./client/scss/**/*.scss"], ['styles', 'inject']);
  gulp.watch(['./client/js/**/*.js', './gulpfile.js'], ['inject']);
  gulp.watch(['./client/pages/**/*.html'], ['html']);
});

gulp.task('default', ['server', 'watch']);
