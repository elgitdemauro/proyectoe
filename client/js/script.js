$(document).ready(function() {

  /*
    Dropdown
  */
  var button = $('.dropdown-button');
  var list = $('.dropdown-list');
  button.on('click', function () {
      $(this).next(list).toggle();
      $(this).toggleClass('active');
  });
  list.children().on('click', function () {
      $(this)
        .parents('.dropdown')
        .find(button)
        .html($(this).html());
      $(this).parent(list).toggle();
      return $(this)
        .parents('.dropdown')
        .find(button).removeClass('active');
  });


  /*
    Collapse
  */
  $(".collap").collapse({
    clickQuery: "span.toggle"
  });
  $('.collap-link').click(function(){
    $(this).hide();
    $('.collap-body').fadeIn('fast');
  });



  /*
    Table slide
  */
  var speed = 5000;
  var item_width = $('#slides .table').width();
  var left_value = item_width * (-1);
  $('#slides .table:first').before($('#slides .table:last'));
  //$('#slides .box').css({'left' : left_value});

  var title = $('#slides .table:last').attr('id') ;
  $('#plan').text(title);

  $('#prev').click(function() {
    var left_indent = parseInt($('#slides .box').css('left')) + item_width;
    $('#slides .box:not(:animated)').animate({'left' : left_indent}, 200,function(){
      $('#slides .table:first').before($('#slides .table:last'));
      $('#slides .box').css({'left' : left_value});
    });
    var title = $('#slides .table:first').attr('id') ;
    $('#plan').text(title);
    return false;
  });

  $('#next').click(function() {
    var left_indent = parseInt($('#slides .box').css('left')) - item_width;
    $('#slides .box:not(:animated)').animate({'left' : left_indent}, 200, function () {
      $('#slides .table:last').after($('#slides .table:first'));
      $('#slides .box').css({'left' : left_value});
    });
    var title = $('#slides .table:first').attr('id') ;
    $('#plan').text(title);
    return false;
  });

  function rotate() {
    $('#next').click();
  };

  var w = $('.slide-body').width();
  var w2= w /2;
  $('.box').css({
    width: w*2,
    left: -w
  });
  $('.table').css({
    width: w
  });



  /*
    Progressbar
  */
  $.fn.progress = function() {
    var percent = this.data("percent");
    this.css("width", percent+"%");
    if(percent > 80){
      setTimeout(function(){
        $('.progressbar-bar').css('background', 'red');
      }, 500);
    }
  };
  $(".progressbar-bar .level").progress();





  /*
    Modal
  */
  $('.trigger-modal').click(function(e){
    e.preventDefault();
    var src = $(this).attr('data-modal');
    console.log(src);
    var modalTarget = $('.'+src);
    modalTarget.show();
    // modal launch slider canales
    if(src == 'canales'){
      $('.bxslider1').bxSlider({
        infiniteLoop: true
      });
    }
  });
  $('.close').click(function(e){
    e.preventDefault();
    $('#screen, #modal').hide();
  });
  // launch slider canales
  $(".col-flex a").click(function(){
    var hre = $(this).attr('href');
    if (hre == '#option2') {
      $('.bxslider2').bxSlider({
        infiniteLoop: true
      });
    } else if(hre == '#option3'){
      $('.bxslider3').bxSlider({
        infiniteLoop: true
      });
    } else if(hre == '#option4'){
      $('.bxslider4').bxSlider({
        infiniteLoop: true
      });
    } else if(hre == '#option5'){
      $('.bxslider5').bxSlider({
        infiniteLoop: true
      });
    } else if(hre == '#option6'){
      $('.bxslider6').bxSlider({
        infiniteLoop: true
      });
    } else if(hre == '#option7'){
      $('.bxslider7').bxSlider({
        infiniteLoop: true
      });
    } else if(hre == '#option8'){
      $('.bxslider8').bxSlider({
        infiniteLoop: true
      });
    } else if(hre == '#option9'){
      $('.bxslider9').bxSlider({
        infiniteLoop: true
      });
    } else if(hre == '#option10'){
      $('.bxslider10').bxSlider({
        infiniteLoop: true
      });
    }
  });


  /*
    Slider example components
  */
  $('.bxslider-components').bxSlider({
    infiniteLoop: true
  });




}); // End ready


$(window).resize(function(){
  var w = $('.slide-body').width();
  var w2= w /2;
  $('.box').css({
    width: w*2,
    left: -w
  });
  $('.table').css({
    width: w
  });
});



/*
  Chart
*/
$(window).resize(function() {
  height = $(".container").height();
  width = $(".container").width();
  $(".container").highcharts().setSize(width, height, doAnimation = false);
  $(".porcentajes").css('width', width);
});

$(document).ready(function(e) {
  $('.container').highcharts({
      chart: {
        type: 'gauge',
        backgroundColor: 'transparent',
        width : 150,
        height : 140
      },
      title: {
        text: ''
      },
      tooltip: {
        enabled: false
      },
      pane: {
        center: ['50%', '50%'],
        size: '100%',
        startAngle: -90,
        endAngle: 90,
        background: {
          backgroundColor: '#025CAE',
          innerRadius: '90%',
          outerRadius: '100%',
          shape: 'arc',
        }
      },
      plotOptions: {
        gauge: {
          dataLabels: {
            enabled: false
          }
        }
      },
      yAxis: {
        minorTickColor: '#025CAE',
        tickWidth: 3,
        tickColor: '#025CAE',
        lineWidth: 0,
        min: 0,
        max: 100,
        tickLength: 15,
        offset: -1,
        labels: {
          enabled: true,
          style: {
            fontSize: 0
          }
        },
      },
      series: [{
        data: [{
          y: 70,
          dial: {
            backgroundColor: '#ff6601',
            radius: '70%',
            baseLength: '2%',
            baseWidth: 12,
            rearLength: '0%',
          }
        }],
      }]
    },
    // Add some life
    function(chart) {
      setInterval(function() {
        newVal = 70;
        chart.yAxis[0].removePlotBand('plot-band-1');
        chart.yAxis[0].addPlotBand({
          from: 0,
          to: newVal,
          color: '#FF7F00',
          thickness: '10%',
          id: 'plot-band-1',
          zIndex: 2
        });
      }, 800);
    });
});

$(window).resize(function() {
  height = $(".container2").height();
  width = $(".container2").width();
  $(".container2").highcharts().setSize(width, height, doAnimation = false);
  $(".porcentajes2").css('width', width);
});

$(document).ready(function(e) {
  $('.container2').highcharts({
      chart: {
        type: 'gauge',
        backgroundColor: 'transparent',
        width : 150,
        height : 140
      },
      title: {
        text: ''
      },
      tooltip: {
        enabled: false
      },
      pane: {
        center: ['50%', '50%'],
        size: '100%',
        startAngle: -90,
        endAngle: 90,
        background: {
          backgroundColor: '#025CAE',
          innerRadius: '90%',
          outerRadius: '100%',
          shape: 'arc',
        }
      },
      plotOptions: {
        gauge: {
          dataLabels: {
            enabled: false
          }
        }
      },
      yAxis: {
        minorTickColor: '#025CAE',
        tickWidth: 3,
        tickColor: '#025CAE',
        lineWidth: 0,
        min: 0,
        max: 100,
        tickLength: 15,
        offset: -1,
        labels: {
          enabled: true,
          style: {
            fontSize: 0
          }
        },
      },
      series: [{
        data: [{
          y: 40,
          dial: {
            backgroundColor: '#FF7F00',
            radius: '70%',
            baseLength: '2%',
            baseWidth: 12,
            rearLength: '0%',
          }

        }],
      }]
    },
    // Add some life
    function(chart) {
      setInterval(function() {
        newVal = 40;
        chart.yAxis[0].removePlotBand('plot-band-1');
        chart.yAxis[0].addPlotBand({
          from: 0,
          to: newVal,
          color: '#FF7F00',
          thickness: '10%',
          id: 'plot-band-1',
          zIndex: 2
        });
      }, 800);
    });
});

$(window).resize(function() {
  height = $(".container3").height();
  width = $(".container3").width();
  $(".container3").highcharts().setSize(width, height, doAnimation = false);
  $(".porcentajes3").css('width', width);
});

$(document).ready(function(e) {
  $('.container3').highcharts({
      chart: {
        type: 'gauge',
        backgroundColor: 'transparent',
        width : 150,
        height : 140
      },
      title: {
        text: ''
      },
      tooltip: {
        enabled: false
      },
      pane: {
        center: ['50%', '50%'],
        size: '100%',
        startAngle: -90,
        endAngle: 90,
        background: {
          backgroundColor: '#025CAE',
          innerRadius: '90%',
          outerRadius: '100%',
          shape: 'arc',
        }
      },
      plotOptions: {
        gauge: {
          dataLabels: {
            enabled: false
          }
        }
      },
      yAxis: {
        minorTickColor: '#025CAE',
        tickWidth: 3,
        tickColor: '#025CAE',
        lineWidth: 0,
        min: 0,
        max: 100,
        tickLength: 15,
        offset: -1,
        labels: {
          enabled: true,
          style: {
            fontSize: 0
          }
        },
      },
      series: [{
        data: [{
          y: 40,
          dial: {
            backgroundColor: '#FF7F00',
            radius: '70%',
            baseLength: '2%',
            baseWidth: 12,
            rearLength: '0%',
          }

        }],
      }]
    },
    // Add some life
    function(chart) {
      setInterval(function() {
        newVal = 40;
        chart.yAxis[0].removePlotBand('plot-band-1');
        chart.yAxis[0].addPlotBand({
          from: 0,
          to: newVal,
          color: '#FF7F00',
          thickness: '10%',
          id: 'plot-band-1',
          zIndex: 2
        });
      }, 800);
    });
});
